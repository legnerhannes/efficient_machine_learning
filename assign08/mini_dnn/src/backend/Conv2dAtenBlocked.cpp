#include "Conv2dAtenBlocked.h"

at::Tensor mini_dnn::backend::Conv2dAtenBlocked::forward(   at::Tensor i_input,
                                                            at::Tensor i_weight ) {
  
  Conv2d::Sizes l_sizes = Conv2d::getSizes( i_input,
                                            i_weight );
  // blocking
  // x = n * cb * h * w * bc
  // w = kb * cb * r * s * bc * bk
  // y = n * kb * p * q * bk
  
  at::Tensor l_output = at::zeros({ l_sizes.n, l_sizes.kb, l_sizes.p, l_sizes.q, l_sizes.bk });

  std::cout << l_output[0][0][0].sizes() << std::endl;
  std::cout << i_input[0][0][0].sizes() << std::endl;
  std::cout << i_weight[0][0][0][0].sizes() << std::endl;


  for( int64_t l_n = 0; l_n < l_sizes.n; l_n++ ) {
    for( int64_t l_kb = 0; l_kb < l_sizes.kb; l_kb++ ) {
      for( int64_t l_p = 0; l_p < l_sizes.p; l_p++ ) {
        for( int64_t l_cb = 0; l_cb < l_sizes.cb; l_cb++ ) {
          for( int64_t l_r = 0; l_r < l_sizes.r; l_r++ ) {
            for( int64_t l_s = 0; l_s < l_sizes.s; l_s++ ) {
              for( int64_t l_h = 0; l_h < l_sizes.h; l_h++ ) {
                
                at::Tensor l_input = i_input[l_n][l_kb][l_h];

                for (int i = 0; i < l_sizes.s - 1; i++) {
                  at::Tensor l_part = at::narrow(l_input, 0, i, l_sizes.q);
                  l_output[l_n][l_kb][l_p] += at::matmul(l_part[0], i_weight[l_kb][l_cb][l_r][l_s]);
                }
              }
            }
          }
        }
      }
    }
  }

  return l_output;
}
#include "Conv2dIm2col.h"

at::Tensor mini_dnn::backend::Conv2dIm2col::forward( at::Tensor i_input,
                                                     at::Tensor i_weight ) {
  // get involved sizes
  Conv2d::Sizes l_sizes = Conv2d::getSizes( i_input,
                                            i_weight );

  // check that we are not having batched data
  MINI_DNN_CHECK_EQ( l_sizes.bc, 1 );
  MINI_DNN_CHECK_EQ( l_sizes.bk, 1 );


  // TODO: finish implementation 
  // X -> n, c, h, w
  // W -> k, c ,r, s
  // Y -> n, k, p, q
  
  at::Tensor l_unfolded_input = at::im2col(i_input, { l_sizes.r, l_sizes.s }, { 1, 1 }, { 0, 0 }, { 1, 1 });

  at::Tensor l_un_in_transposed = l_unfolded_input.transpose(1, 2);
  at::Tensor l_unfolded_weight_view = i_weight.view({ i_weight.size(0), -1 }).t();

  at::Tensor l_unfolded_output = l_un_in_transposed.matmul(l_unfolded_weight_view).transpose(1, 2);

  at::Tensor l_output = at::col2im(l_unfolded_output, { l_sizes.p, l_sizes.q }, { 1, 1 }, { 1, 1 }, { 0, 0 }, { 1, 1 });

  return l_output;
}

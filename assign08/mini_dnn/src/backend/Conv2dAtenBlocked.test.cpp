#include <catch2/catch.hpp>
#include "Conv2dAtenBlocked.h"

TEST_CASE( "Tests the convolution operator going through blocked im2col + sgemm.",
           "[conv2d][blocking][forward]" ) {
  
  std::cout << "test blocking" << std::endl;

  // sizes
  int64_t l_size_n = 18;
  int64_t l_size_h = 24;
  int64_t l_size_w = 36;
  int64_t l_size_c = 25;

  int64_t l_size_k = 16;
  int64_t l_size_r = 21;
  int64_t l_size_s = 9;

  int64_t l_size_p = l_size_h - (l_size_r - 1);
  int64_t l_size_q = l_size_w - (l_size_s - 1);

  std::cout << "SIZE P " << l_size_p << std::endl;
  std::cout << "SIZE Q " << l_size_q << std::endl;

  // block sizes
  int64_t l_size_bc = 5;
  int64_t l_size_bk = 4;


  // block counts
  int64_t l_size_cb = l_size_c / l_size_bc;
  int64_t l_size_kb = l_size_k / l_size_bk;
  

  // construct input and weight tensors
  at::Tensor l_input = at::rand( {l_size_n, l_size_c, l_size_h , l_size_w} );
  at::Tensor l_weight = at::rand( {l_size_k, l_size_c, l_size_r, l_size_s} );

  // blocking
  // x = n * cb * h * w * bc
  // w = kb * cb * r * s * bc * bk
  // y = n * kb * p * q * bk

  at::Tensor l_blocked_x = at::zeros( {l_size_n, l_size_cb, l_size_h , l_size_w, l_size_bc} );
  at::Tensor l_blocked_w = at::zeros( {l_size_kb, l_size_cb, l_size_r, l_size_s, l_size_bc, l_size_bk} );

  // block x
  for (int l_n = 0; l_n < l_size_n; l_n++) {
    for (int l_cb = 0; l_cb < l_size_cb; l_cb++) {
      for (int l_h = 0; l_h < l_size_h; l_h++) {
        for (int l_w = 0; l_w < l_size_w; l_w++) {
          for (int l_bc = 0; l_bc < l_size_bc; l_bc++) {
            l_blocked_x[l_n][l_cb][l_h][l_w][l_bc] = l_input[l_n][l_cb * l_size_bc + l_bc][l_h][l_w];
          }
        }
      }
    }
  }

  // block w
  for (int l_kb = 0; l_kb < l_size_kb; l_kb++) {
    for (int l_cb = 0; l_cb < l_size_cb; l_cb++) {
      for (int l_r = 0; l_r < l_size_r; l_r++) {
        for (int l_s = 0; l_s < l_size_s; l_s++) {
          for (int l_bc = 0; l_bc < l_size_bc; l_bc++) {
            for (int l_bk = 0; l_bk < l_size_bk; l_bk++) {
              l_blocked_w[l_kb][l_cb][l_r][l_s][l_bc][l_bk] = l_weight[l_kb * l_size_bk + l_bk][l_cb * l_size_bc + l_bc][l_r][l_s];
            }
          }
        }
      }
    }
  }


  // compute solution
  mini_dnn::backend::Conv2dAtenBlocked l_conv2d;
  at::Tensor l_blocked_y = l_conv2d.forward(  l_blocked_x,
                                              l_blocked_w );


  at::Tensor l_output = at::zeros( {l_size_n, l_size_k, l_size_p, l_size_q} );

  // unblock y
  for (int l_n = 0; l_n < l_size_n; l_n++) {
    for (int l_kb = 0; l_kb < l_size_kb; l_kb++) {
      for (int l_p = 0; l_p < l_size_p; l_p++) {
        for (int l_q = 0; l_q < l_size_q; l_q++) {
          for (int l_bk = 0; l_bk < l_size_bk; l_bk++) {
            l_output[l_n][l_kb * l_size_bk + l_bk][l_p][l_q] = l_blocked_y[l_n][l_kb][l_p][l_q][l_bk];
          }
        }
      }
    }
  }


  // compute reference
  at::Tensor l_reference = at::conv2d( l_input,
                                       l_weight );

  // check solution
  REQUIRE( at::allclose( l_output, l_reference ) );
}
import eml.linear_cpp.function as func
from torch import nn

class Layer(nn.Module):
    def __init__( self,
              i_n_features_input,
              i_n_features_output ):
        self.m_features_input = i_n_features_input 
        self.m_features_output = i_n_features_output


    def forward(self, input, weight, bias):
        inputs = [input, weight, bias]
        self.m_output =  func.Function.apply(input, weight, bias)
        return self.m_output

    def backward(self, grad_output):
        return self.m_output.backward(grad_output)

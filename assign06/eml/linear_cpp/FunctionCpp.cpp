#include <pybind11/pybind11.h>
#include <torch/extension.h>
#include <iostream>


torch::Tensor forward( torch::Tensor i_input,
                       torch::Tensor i_weight ) {

  torch::Tensor l_transposed_weight = torch::transpose(i_weight, 0, 1);
  torch::Tensor l_result = torch::matmul(i_input, l_transposed_weight);
  return l_result;
}

std::vector< torch::Tensor > backward( torch::Tensor i_grad,
                                       torch::Tensor i_input,
                                       torch::Tensor i_weights ) {

  std::vector<torch::Tensor> l_result;
  l_result.push_back(torch::matmul(i_grad, i_weights));

  torch::Tensor l_transposed_grad = torch::transpose(i_grad, 0, 1);
  l_result.push_back(torch::matmul(l_transposed_grad, i_input));

  return l_result;
}


void hello() {
  std::cout << "hello world!" << std::endl;
}

PYBIND11_MODULE( TORCH_EXTENSION_NAME,
                 io_module) {
  io_module.def( "hello_world",
                 &hello,
                 "My Hello world function!");

  io_module.def( "forward_pass",
                 &forward,
                 "Forward pass C++ implementation.");

  io_module.def( "backward_pass",
                 &backward,
                 "Backward pass C++ implementation.");
}
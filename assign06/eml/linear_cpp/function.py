import torch
import cpp_functions

class Function(torch.autograd.Function):
    @staticmethod
    def forward(input, weight, bias):
        return cpp_functions.forward_pass(input.contiguous(), weight.contiguous())

    @staticmethod
    def setup_context(ctx, inputs, output):
        input, weight, bias = inputs
        ctx.save_for_backward(input, weight, bias)


    @staticmethod
    def backward(ctx, grad_output):
        input, weight, bias = ctx.saved_tensors
        return cpp_functions.backward_pass(grad_output.contiguous(), input.contiguous(), weight.contiguous())
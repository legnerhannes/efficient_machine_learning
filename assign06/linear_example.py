import torch


# 6.1
l_a = torch.tensor( [ [1, 2, 3],
                      [4, 5, 6] ],
                    dtype = torch.float32,
                    requires_grad = True )

l_x = torch.tensor( [ [ 7,  8,  9],
                      [10, 11, 12] ],
                    dtype = torch.float32,
                    requires_grad = True )

# TASK 2
# constructing linear layer with parameters in_features = 3, out_features = 2 and bias = False
l_linear_torch = torch.nn.Linear(   3,
                                    2,
                                    bias = False )

l_linear_torch.weight = torch.nn.Parameter( l_a )

# TASK 3
# applying forward function
l_result = l_linear_torch.forward( l_x )


# TASK 4
# run backward pass
l_grad_y = torch.tensor(    [ [1, 2],
                              [2, 3] ],
                            dtype = torch.float32 )

l_result.backward( l_grad_y )

# forward result
print(l_x)
print(l_result)


# backward result
print(l_x.grad)
print(l_linear_torch.weight.grad)
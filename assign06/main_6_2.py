import eml.ext.linear_python.layer as lp
import torch

l_input = torch.tensor( [ [ 7,  8,  9],
                      [10, 11, 12] ],
                    dtype = torch.float32,
                    requires_grad = True )

l_weight = torch.tensor( [ [1, 2, 3],
                      [4, 5, 6] ],
                    dtype = torch.float32,
                    requires_grad = True )

l_grad_y = torch.tensor(    [ [1, 2],
                              [2, 3] ],
                            dtype = torch.float32 )


l_layer = lp.Layer(3, 2)

print("FORWARD")
print(l_layer.forward(l_input, l_weight, None))

print("BACKWARD")
print(l_layer.backward(l_grad_y))
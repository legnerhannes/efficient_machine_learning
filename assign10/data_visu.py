import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def render_slice(image_slice):
    plt.imshow(image_slice, cmap='gray')
    plt.axis('off')
    plt.show()



# main

data = np.load('./data_train.npz')
image = data['data']
index = int(len(image) / 2)

render_slice(image[:, index, :])
render_slice(image[:, :, index])






#include <cstdlib>
#include <ATen/ATen.h>
#include <iostream>

using namespace std;

int main() {
  std::cout << "running the ATen examples" << std::endl;

  float l_data[4*2*3] = {  0.0f,  1.0f,  2.0f, 
                           3.0f,  4.0f,  5.0f,

                           6.0f,  7.0f,  8.0f, 
                           9.0f, 10.0f, 11.0f,
                           
                          12.0f, 13.0f, 14.0f,
                          15.0f, 16.0f, 17.0f,
                          
                          18.0f, 19.0f, 20.0f,
                          21.0f, 22.0f, 23.0f };

  std::cout << "l_data (ptr): " << l_data << std::endl;
  
  cout << "\n======STORAGE======" << endl;

  at::Tensor l_tensor = at::from_blob(l_data, {4, 2, 3} );
  cout << l_tensor << endl;

  cout << "\n======metadata======" << endl;
  cout << l_tensor.dtype() << endl;
  cout << l_tensor.data_ptr() << endl;
  cout << l_tensor.sizes() << endl;
  cout << l_tensor.strides() << endl;
  cout << l_tensor.storage_offset() << endl;
  cout << l_tensor.device() << endl;
  cout << l_tensor.layout() << endl;
  cout << l_tensor.is_contiguous() << endl;

  cout << "\n======chaning tensor======" << endl;
  // both operations change the respective element they are pointing to
  l_data[6] = l_data[6] * 2.0f;
  l_tensor[1][0][1] = 9.0f;
  cout << l_tensor << endl;


  cout << "\n======view======" << endl;
  at::Tensor l_view = l_tensor.select(1, 1);
  cout << l_view << endl;

  // value is changed with one operation for both tensor objects -> both use same memory
  l_data[9] = l_data[9] * 3.0f;
  cout << l_tensor << endl;
  cout << l_view << endl;


  cout << "\n======contiguous======" << endl;
  // this operation uses contiguous memory adresses and also callocates new memory fpr l_cont
  at::Tensor l_cont = l_view.contiguous();
  cout << l_cont << endl;

  // l_view changes, l_cont stays the same becauseit doesn't use memory of l_data anymore
  l_data[10] = l_data[10] * 2.0f;
  cout << l_view << endl;
  cout << l_cont << endl;



  cout << "\n======OPERATIONS======" << endl;

  cout << "\n======init a and b======" << endl;
  at::Tensor l_a = at::rand({16, 4});
  at::Tensor l_b = at::rand({4, 16});
  cout << l_a << endl;
  cout << l_b << endl;


  cout << "\n======matmul======" << endl;
  cout << at::matmul(l_a, l_b) << endl;


  cout << "\n======init t0 and t1======" << endl;
  at::Tensor l_t0 = at::rand({16, 4, 2});
  at::Tensor l_t1 = at::rand({16, 2, 4});
  cout << l_t0 << endl;
  cout << l_t1 << endl;


  cout << "\n======bmm======" << endl;
  cout << at::bmm(l_t0, l_t1) << endl;

  std::cout << "finished running ATen examples" << std::endl;
  return EXIT_SUCCESS;
}

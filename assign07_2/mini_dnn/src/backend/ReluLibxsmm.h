#ifndef MINI_DNN_BACKEND_RELU_LIBXSMM_H
#define MINI_DNN_BACKEND_RELU_LIBXSMM_H

#include "Matmul.hpp"
#include <ATen/ATen.h>

namespace mini_dnn {
  namespace backend {
    class ReluLibxsmm;
  }
}

/**
 * Matmul backend using LIBXSMM.
 **/
class mini_dnn::backend::ReluLibxsmm {
  private:
  public:
    /**
     * Perform the forward pass, i.e., Y = XW.
     *
     * @param i_tensor input tensor
     * @return output of the matmul, i.e., Y.
     **/
    at::Tensor forward( at::Tensor i_tensor );

    static uint64_t nOps( at::Tensor i_tensor) {

      uint64_t l_n_flops = 1;
      l_n_flops *= i_tensor.size(0) * i_tensor.size(1);
      l_n_flops *= 2;

      return l_n_flops;
    }
};

#endif
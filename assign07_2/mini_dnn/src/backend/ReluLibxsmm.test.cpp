#include <catch2/catch.hpp>
#include "ReluLibxsmm.h"

TEST_CASE( "Tests the Relu forward operator through blocked Aten calls.",
           "[relu][libxsmm][forward]" ) {

  // sizes of the input
  int64_t l_size_m = 12;
  int64_t l_size_n = 8;

  // construct input tensor
  at::Tensor l_tensor = at::rand( { l_size_m, l_size_n } );
  l_tensor = at::negative(l_tensor);

  std::cout << l_tensor << std::endl;

  mini_dnn::backend::ReluLibxsmm relu;
  at::Tensor l_result = relu.forward(l_tensor);

  std::cout << l_result << std::endl;
}
#include "ReluLibxsmm.h"
#include <libxsmm.h>

at::Tensor mini_dnn::backend::ReluLibxsmm::forward( at::Tensor i_tensor ) {

  // create LIBXSMM kernel (isolated)
  libxsmm_gemm_shape l_shape_brgemm;
  libxsmm_bitfield l_flags_brgemm = LIBXSMM_GEMM_FLAGS('N', 'N');
  libxsmm_bitfield l_prefetch_flags_brgemm = 0;
  
  libxsmm_blasint l_m = i_tensor.size(0);
  libxsmm_blasint l_n = i_tensor.size(1);

  libxsmm_meltw_unary_shape l_unary_shape = libxsmm_create_meltw_unary_shape( l_m,
                                                                              l_n,
                                                                              l_m,
                                                                              l_m,
                                                                              LIBXSMM_DATATYPE_F32,
                                                                              LIBXSMM_DATATYPE_F32,
                                                                              LIBXSMM_DATATYPE_F32 );



  libxsmm_meltwfunction_unary l_relu = libxsmm_dispatch_meltw_unary_v2( LIBXSMM_MELTW_TYPE_UNARY_RELU,
                                                                        l_unary_shape,
                                                                        LIBXSMM_MELTW_FLAG_UNARY_NONE );


  // prepare data for blocked LIBXSMM calls
  at::Tensor o_tensor = at::zeros( { l_m, l_n } );

  c10::IntArrayRef l_strides_a = i_tensor.strides();
  c10::IntArrayRef l_strides_b = o_tensor.strides();

  float * l_ptr_i = (float*) i_tensor.data_ptr();
  float * l_ptr_o = (float*) o_tensor.data_ptr();


  libxsmm_meltw_unary_param l_param;
  l_param.in.primary = l_ptr_i;
  l_param.out.primary = l_ptr_o;

  l_relu( &l_param );

  return o_tensor;
}
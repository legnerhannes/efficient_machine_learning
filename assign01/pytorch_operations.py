import torch


# ===== Task 1 =====

print("basic add and mul")

# Generieren von P und Q via der Liste von Listen zu Tensor Methode
py0 = [0,1,2]
py1 = [3,4,5]
px0 = [py0,py1]

qy0 = [6,7,8]
qy1 = [9,10,11]
qx0 = [qy0,qy1]

p = torch.tensor(px0)
q = torch.tensor(qx0)

# Addition und Multiplikation von P und Q
print(torch.add(p, q))
print(p + q)
print(torch.mul(p, q))
print(p * q)


# ===== Task 2 =====

print("\nmatrix mul")

# Transponieren der ersten und zweiten Dimension des Q Tensors, damit Matrixmultiplikation zwischen Q und P möglich ist.
qt = torch.transpose(q, 0, 1)
print(qt)

# Matrixmultiplikation von P und Q
print(torch.matmul(p, qt))
print(p @ qt)


# ===== Task 3 =====

print("\nreduction operations")

# Gibt die Summe aller Werte des Tensors P aus.
print(torch.sum(p))

# Gibt den Maximalwert, welcher im Tenor P enthalten st an.
print(torch.max(p))


# ===== Task 4 =====

print("\ntensor clone")

# l_tmp zeigt auf die gleichen Speicheradressen wie P -> print Statement hier gibt 0 Tensor aus
l_tmp = p
l_tmp[:] = 0
print(p)

# l_tmp zeigt auf eine Kopie vom Tensor Q, welche eigene Speicheradressen hat -> print Statement gibt Tensor Q aus
l_tmp = q.clone().detach()
l_tmp[:] = 0
print(q)
import torch
import ctypes

# Funktion um die Metadaten der verschiedenen Tasks auszugeben
def printMetadata(tensor):
    print(tensor)
    print(tensor.size())
    print(tensor.stride())
    print(tensor.dtype)
    print(tensor.layout)
    print(tensor.device)


# ===== Task 1 =====

print("basic tensor attributes")

# Generieren des gegebenen Tensors und Ausgabe durch Metadaten Funktion
z0 = [ 0, 1, 2]
z1 = [ 3, 4, 5]
z2 = [ 6, 7, 8]
z3 = [ 9,10,11]
z4 = [12,13,14]
z5 = [15,16,17]
z6 = [18,19,20]
z7 = [21,22,23]

y0 = [z0,z1]
y1 = [z2,z3]
y2 = [z4,z5]
y3 = [z6,z7]

x0 = [y0,y1,y2,y3]

t = torch.tensor(x0)
printMetadata(t)


# ===== Task 2 =====

# Änderung des dtypes zu float32
print("\nchange dtype")
l_tensor_float = t.float()
print(l_tensor_float.dtype)


# ===== Task 3 =====

#  Stride, Size und der in Task 2 veränderte dtype verändern sich
print("\nfix dimension")
l_tensor_fixed = l_tensor_float[:,0,:]
printMetadata(l_tensor_fixed)


# ===== Task 4 =====

# complex_view hat eine Größe von 2, 3, weil von 2 Einträgen der ersten Dimension, die 3 Einträge aus den 2. Eintrag der 2 Dimension dargestellt wird.
# Stripe ist (12,1) -> 3 Einträge in Zeilen liegen direkt hintereinander im Speicher
#                   -> 6 Speicherplätze liegen zwischen Elementen an gleicher Stelle in der 1. Dimension -> 12 da in der 1. Dimension nur jeder 2. Eintrag enthlten ist
print("\ncomplex view")
l_tensor_complex_view = l_tensor_float[::2,1,:]
printMetadata(l_tensor_complex_view)


# ===== Task 5 =====

# Die ncontiguous-Funktion entfernt füllt praktisch die Lücken im Speicher auf, damit die Daten der Tensor view direkt hintereinander im Speicher stehen. Daher ist der Stride bei 3, da zwischen 3 und 15 nur 3 Speicheradressen liegen
print("\ncontiguous function")
l_tensor_complex_view = l_tensor_complex_view.contiguous()
printMetadata(l_tensor_complex_view)


# ===== Task 6 =====

# Nutzung von for Schleife, um durch die Speicheradressen des Tensors zu iterieren
print("\nprinting internal data")
t_mem_adress = l_tensor_complex_view.data_ptr()

for i in range (0, 4*2*3):
    l_data_ptr = t_mem_adress + i
    l_data_raw = (ctypes.c_float).from_address(l_data_ptr)
    print(l_data_raw)
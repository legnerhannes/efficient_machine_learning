import torch
import numpy


# ===== Task 1 =====

print("torch zeros")
# Generiert mit 0 gefüllten Tensor in den angegebenen Dimensionen.
zero_tensor = torch.zeros(2,2,4)
print(zero_tensor)


print("\ntorch ones")
# Generiert mit 1 gefüllten Tensor in den angegebenen Dimensionen.
one_tensor = torch.ones(2,3,4,3)
print(one_tensor)


print("\torch random")
# Generiert mit zufälligen Werten zwischen 0 und 1 gefüllten Tensor in den angegebenen Dimensionen.
random_torch = torch.rand(4,2,3)
print(random_torch)


print("\ntorch ones like")
# Generiert mit 1 gefüllten Tensor in den Dimensionen des übergebenen Tensors.
one_like_tensor = torch.ones_like(random_torch)
print(one_like_tensor)


# ===== Task 2 =====

print("\nlist of lists to tensor")

# 3. Dimension
z0 = [9,6,3]
z1 = [2,5,7]
z2 = [2,2,1]
z3 = [5,3,2]
z4 = [9,0,9]
z5 = [1,2,3]
z6 = [1,8,7]
z7 = [6,3,4]

# 2. Dimension
y0 = [z0,z1]
y1 = [z2,z3]
y2 = [z4,z5]
y3 = [z6,z7]

# 1. Dimension --> x0 enthält daten von kompletten Tensor in Form einer Liste von Listen von Listen
x0 = [y0,y1,y2,y3]

# Python Liste in Tensor Objekt parsen
list_tensor = torch.tensor(x0)
print(list_tensor)


# ===== Task 3 =====

print("\nnumpy array to tensor")

# Python Liste zu Numpy Array Parsen und diesen dann zum Tensor parsen.
num_array = numpy.array(x0)
numpy_tensor = torch.tensor(num_array)

print(num_array)
print(numpy_tensor)
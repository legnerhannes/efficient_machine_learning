## 1. Remote Machine Learning

Output von lscpu auf dem Draco Cluster:

> Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
CPU(s):              96
On-line CPU(s) list: 0-95
Thread(s) per core:  2
Core(s) per socket:  24
Socket(s):           2
NUMA node(s):        2
Vendor ID:           GenuineIntel
CPU family:          6
Model:               106
Model name:          Intel(R) Xeon(R) Platinum 8360Y CPU @ 2.40GHz
Stepping:            6
CPU MHz:             2600.000
CPU max MHz:         3500,0000
CPU min MHz:         800,0000
BogoMIPS:            5200.00
L1d cache:           48K
L1i cache:           32K
L2 cache:            1280K
L3 cache:            55296K
NUMA node0 CPU(s):   0-23,48-71
NUMA node1 CPU(s):   24-47,72-95
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb cat_l3 invpcid_single ssbd mba ibrs ibpb stibp ibrs_enhanced fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid cqm rdt_a avx512f avx512dq rdseed adx smap avx512ifma clflushopt clwb intel_pt avx512cd sha_ni avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local split_lock_detect wbnoinvd dtherm ida arat pln pts hwp hwp_act_window hwp_epp hwp_pkg_req avx512vbmi umip pku ospke avx512_vbmi2 gfni vaes vpclmulqdq avx512_vnni avx512_bitalg tme avx512_vpopcntdq la57 rdpid fsrm md_clear pconfig flush_l1d arch_capabilities


MemInfo:

> MemTotal:       263775976 kB
MemFree:        179720652 kB
MemAvailable:   259094180 kB
Buffers:            4048 kB
Cached:         80037360 kB
SwapCached:            0 kB
Active:         43396172 kB
Inactive:       37151532 kB
Active(anon):     415360 kB
Inactive(anon):   551568 kB
Active(file):   42980812 kB
Inactive(file): 36599964 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:                12 kB
Writeback:             0 kB
AnonPages:        503660 kB
Mapped:            95332 kB
Shmem:            460632 kB
KReclaimable:    1645080 kB
Slab:            2510020 kB
SReclaimable:    1645080 kB
SUnreclaim:       864940 kB
KernelStack:       17408 kB
PageTables:        11508 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    131887988 kB
Committed_AS:    1367060 kB
VmallocTotal:   13743895347199 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
Percpu:           152064 kB
HardwareCorrupted:     0 kB
AnonHugePages:    405504 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
FileHugePages:         0 kB
FilePmdMapped:         0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
DirectMap4k:     2020164 kB
DirectMap2M:    207360000 kB
DirectMap1G:    60817408 kB


## 2 Tensors

### 2.1 Python

Die Lösungen zu den Aufgaben und Kommentare sind in den Skripten **pytorch_creation.py**, **pytorch_operation.py** und **pytorch_creation.py** zu finden.
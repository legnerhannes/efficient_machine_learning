import torch
from torchvision.models import MobileNetV2
from aimet_common.defs import QuantScheme
from aimet_torch.quantsim import QuantizationSimModel


def callibration(i_sim_model, forward_pass_args = None):
    l_data = torch.randn(32, 3, 3, 3)
    i_sim_model( l_data )

    
# main

l_model = MobileNetV2()


l_input_shape = (1, 3, 224, 224)
l_dummy_input = torch.randn(l_input_shape)

l_quant_sim = QuantizationSimModel( l_model, l_dummy_input,
                                    quant_scheme = QuantScheme.post_training_tf_enhanced,
                                    default_param_bw = 8, default_output_bw = 8)


l_quant_sim.compute_encodings(callibration, None)

print("Export")


l_quant_sim.export('./export_model', 'MobileNetV2', l_dummy_input)
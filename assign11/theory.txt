compute encodings:

Eine Funktion der QuantizationSimModel Klasse. Nachdem man eine Instanz der Klasse mit seinen Model initialisiert hat, kann man über diese Funktion die Quantifizierung durchführen. Dabei werden alle Werte der Knoten des Models auf entsprechende Werte kodiert, abhängig von den übergabeparametern bei der Erstellung der Klasse.


forward_pass_callback:

In diesen Parameter soll eine Funktion übergeben werden, welche den Forward Pass des Models ausführt. Die benutzten Datasamples werden intern von der Funktion ausgewählt, weshalb die in der Funktion übergebenen Daten repräsentativ für die gesamten Daten sein sollten.
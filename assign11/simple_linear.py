import torch
from aimet_common.defs import QuantScheme
from aimet_torch.quantsim import QuantizationSimModel

class SimpleLinear( torch.nn.Module ):
  def __init__( self ):
    super( SimpleLinear, self ).__init__()

    self.m_layer = torch.nn.Linear( in_features = 4,
                                    out_features = 4,
                                    bias = False )

  def forward( self,
               i_input ):
    l_result = self.m_layer( i_input )
    return l_result

def calibrate( io_model,
               i_use_cuda = False ):
  l_data = torch.Tensor( [0.61, -0.93, 0.71, 0.19] )
  io_model( l_data )

if __name__ == "__main__":
  print( 'Running simple linear quantization example' )

  l_model = SimpleLinear()
  
  # freeze model
  l_model = l_model.eval()
  for l_pa in l_model.parameters():
    print(l_pa)
    l_pa.requires_grad = False

  l_w = torch.tensor( [ [ 0.25, -0.32,  1.58,  2.10],
                        [-1.45,  1.82, -0.29,  3.78],
                        [-2.72, -0.12,  2.24, -1.84],
                        [ 1.93,  0.49,  0.00, -3.19] ],
                        requires_grad = False )

  l_model.m_layer.weight = torch.nn.Parameter( l_w,
                                               requires_grad = False )
  print( l_model )

  l_x = torch.tensor( [0.25, 0.17, -0.31, 0.55] )

  print( 'FP32 Result:')
  l_y = l_model( l_x )
  print( l_y )

  

  # quantization
  l_quant_sim = QuantizationSimModel(l_model, l_x, quant_scheme = QuantScheme.post_training_tf, default_output_bw = 4, default_param_bw = 4)

  l_quant_sim.compute_encodings(calibrate, None)


  print('Quantized result Result:')
  l_y = l_model( l_x )
  print( l_y )


  print("MIN")
  print(l_quant_sim)

  # input
  #   min = -0.9839999914169312
  #   max = 0.6559999942779541
  #   delta = 0.10933333237965902

  # weight
  #   min = -4.319999967302595
  #   max = 3.7799999713897705
  #   delta = 0.5399999959128243

  # output
  #   min = -2.252160135904948
  #   max = 1.9706401189168292
  #   delta = 0.2815200169881185

  print( "export model")

  l_quant_sim.export('./export_linear', 'quantized_simple_linear', l_x)


  print( 'finished' )
import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor

import datetime
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

import model
import trainer
import tester
import visualizer

# matplotlib to pdf function for 4.1
def randomVisuOfData(data):
    labels_map = {
        0: "T-Shirt",
        1: "Trouser",
        2: "Pullover",
        3: "Dress",
        4: "Coat",
        5: "Sandal",
        6: "Shirt",
        7: "Sneaker",
        8: "Bag",
        9: "Ankle Boot",
    }

    with PdfPages('./vis/samples/sample.pdf') as pdf:
        figure = plt.figure(figsize=(8, 8))
        cols, rows = 3, 3
        for i in range(1, cols * rows + 1):
            sample_idx = torch.randint(len(data), size=(1,)).item()
            img, label = data[sample_idx]
            figure.add_subplot(rows, cols, i)
            plt.title(labels_map[label])
            plt.axis("off")
            plt.imshow(img.squeeze(), cmap="gray")
        

        pdf.savefig()  # saves the current figure into a pdf page
        plt.close()


# 4.1 DATA SETS AND DATA LOADERS
# Download training data from open datasets.
training_data = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
)

# Download test data from open datasets.
test_data = datasets.FashionMNIST(
    root="data",
    train=False,
    download=True,
    transform=ToTensor(),
)

# Call function to visualize random data sample of training data
randomVisuOfData(training_data)

# Create data loaders.
batch_size = 64
train_dataloader = DataLoader(training_data, batch_size=batch_size)
test_dataloader = DataLoader(test_data, batch_size=batch_size)
visu_dataloader = DataLoader(test_data, batch_size=1)


# 4.2 TRAINING AND VALIDATION
nn_model = model.Model()
loss_func = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(nn_model.parameters(), lr = 1E-1)

epochs = 100

visu_stride = 10
visualizer.plot(0, 500, visu_dataloader, nn_model, "./vis/results/epoch" + str(0) + ".pdf")

for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    trainer.train(loss_func, train_dataloader, nn_model, optimizer)
    tester.test(loss_func, test_dataloader, nn_model)

    # 4.3 VISUALIZATION
    if (t + 1) % visu_stride == 0:
        visualizer.plot(0, 500, visu_dataloader, nn_model, "./vis/results/epoch" + str(t + 1) + ".pdf")

print("Done!")

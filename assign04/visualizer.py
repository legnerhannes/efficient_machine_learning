import torch
import matplotlib.pyplot as plt

## Converts an Fashion MNIST numeric id to a string.
#  @param i_id numeric value of the label.
#  @return string corresponding to the id.
def toLabel( i_id ):
  l_labels = [ "T-Shirt",
               "Trouser",
               "Pullover",
               "Dress",
               "Coat",
               "Sandal",
               "Shirt",
               "Sneaker",
               "Bag",
               "Ankle Boot" ]
  
  return l_labels[i_id]

## Applies the model to the data and plots the data.
#  @param i_off offset of the first image.
#  @param i_stride stride between the images.
#  @param io_data_loader data loader from which the data is retrieved.
#  @param io_model model which is used for the predictions.
#  @param i_path_to_pdf optional path to an output file, i.e., nothing is shown at runtime.
def plot( i_off,
          i_stride,
          io_data_loader,
          io_model,
          i_path_to_pdf = None ):
  
  # switch to evaluation mode
  io_model.eval()

  # create pdf if required
  if( i_path_to_pdf != None ):
    import matplotlib.backends.backend_pdf
    l_pdf_file = matplotlib.backends.backend_pdf.PdfPages( i_path_to_pdf )

  # get list of samples to plot
  sample_images = []
  current = i_off
  index = 0

  for X, y in io_data_loader:
    if index == current:
      current += i_stride
      sample_images.append(X)
      if len(sample_images) == 9:
        break
      
    index += 1  

  # plot the samples
  figure = plt.figure(figsize=(8, 8))
  cols, rows = 3, 3
  index = 0

  for i in range(1, cols * rows + 1):
    pred = io_model(sample_images[index])
    figure.add_subplot(rows, cols, i)
    plt.title(toLabel(pred.argmax(1)))
    plt.axis("off")
    plt.imshow(sample_images[index].squeeze(), cmap="gray")

    index += 1

  l_pdf_file.savefig()  # saves the current figure into a pdf page
  plt.close()

  # close pdf if required
  if( i_path_to_pdf != None ):
    l_pdf_file.close()

  print("Saved file " + i_path_to_pdf)
import torch

## Tests the model
#  @param i_loss_func used loss function.
#  @param io_data_loader data loader containing the data to which the model is applied.
#  @param io_model model which is tested.
#  @return summed loss over all test samples, number of correctly predicted samples.

def test( i_loss_func,
          io_data_loader,
          io_model ):

  # switch model to evaluation mode
  io_model.eval()

  l_loss_total = 0
  l_n_correct = 0

  size = len(io_data_loader.dataset)
  num_batches = len(io_data_loader)

  # add to total loss and count correct predictions
  with torch.no_grad():
    for X, y in io_data_loader:
      pred = io_model(X)
      l_loss_total += i_loss_func(pred, y).item()
      l_n_correct += (pred.argmax(1) == y).type(torch.float).sum().item()

  l_loss_total /= num_batches
  l_n_correct /= size
  print(f"Test Error: \n Accuracy: {(100*l_n_correct):>0.1f}%, Avg loss: {l_loss_total:>8f} \n")

  return l_loss_total, l_n_correct

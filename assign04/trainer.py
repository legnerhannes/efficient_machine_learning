## Trains the given MLP-model.
#  @param i_loss_func used loss function.
#  @param io_data_loader data loader containing the data to which the model is applied (single epoch).
#  @param io_model model which is trained.
#  @param io_optimizer.
#  @return summed loss over all training samples.
def train( i_loss_func,
           io_data_loader,
           io_model,
           io_optimizer ):

  # switch model to training mode
  io_model.train()

  l_loss_total = 0

  size = len(io_data_loader.dataset)
  for batch, (X, y) in enumerate(io_data_loader):
    # Compute prediction error
    pred = io_model(X)
    loss = i_loss_func(pred, y)

    # add up loss
    l_loss_total += loss

    # Backpropagation
    io_optimizer.zero_grad()
    loss.backward()
    io_optimizer.step()

    if batch % 100 == 0:
        loss, current = loss.item(), (batch + 1) * len(X)
        print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

  return l_loss_total
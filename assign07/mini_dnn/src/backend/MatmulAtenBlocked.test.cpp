#include <catch2/catch.hpp>
#include "MatmulAtenBlocked.h"
#include <iostream>

TEST_CASE( "Tests the Matmul forward operator through blocked Aten calls.",
           "[matmul][aten_blocked][forward]" ) {
  // BLAS -> Deep Learning:
  // M: N (batch size)
  // K: C (in features)
  // N: K (out features)

  // sizes of the input
  int64_t l_size_n = 128;
  int64_t l_size_k = 256;
  int64_t l_size_c = 512;

  int64_t l_size_bn =  64;
  int64_t l_size_bk =  32;
  int64_t l_size_bc = 128;

  int64_t l_size_nb = l_size_n / l_size_bn;
  int64_t l_size_kb = l_size_k / l_size_bk;
  int64_t l_size_cb = l_size_c / l_size_bc;

  // construct input tensors
  at::Tensor l_x = at::rand( { l_size_n, l_size_c } );
  at::Tensor l_w = at::rand( { l_size_c, l_size_k } );

  // TODO:
  //   1) derive blocked X and W
  //   2) compute blocked solution through MatmulAtenBlocked.forward
  //   3) reverse blocking and verify


  // derive blocked X
  at::Tensor l_blocked_x = at::zeros({l_size_nb, l_size_cb, l_size_bc, l_size_bn});

  for (int nb = 0; nb < l_size_nb; nb++) {
    for (int cb = 0; cb < l_size_cb; cb++) {
      for (int bc = 0; bc < l_size_bc; bc++) {
        for (int bn = 0; bn < l_size_bn; bn++) {
          l_blocked_x[nb][cb][bc][bn] = l_x[nb * l_size_bn + bn][cb * l_size_bc + bc];
        }
      }
    }
  }


  // derive blocked W
  at::Tensor l_blocked_w = at::zeros({l_size_kb, l_size_cb, l_size_bk, l_size_bc});

  for (int kb = 0; kb < l_size_kb; kb++) {
    for (int cb = 0; cb < l_size_cb; cb++) {
      for (int bk = 0; bk < l_size_bk; bk++) {
        for (int bc = 0; bc < l_size_bc; bc++) {
          l_blocked_w[kb][cb][bk][bc] = l_w[cb * l_size_bc + bc][kb * l_size_bk + bk];
        }
      }
    }
  }
  

  std::cout << l_blocked_x.sizes() << std::endl;
  std::cout << l_blocked_w.sizes() << std::endl;  

  // calculate blocked y
  mini_dnn::backend::MatmulAtenBlocked matmul;
  at::Tensor l_blocked_y = matmul.forward(l_blocked_x, l_blocked_w);
  std::cout << l_blocked_y.sizes() << std::endl;  


  // unblock y
  at::Tensor l_y = at::rand( { l_size_n, l_size_k } );

  for (int kb = 0; kb < l_size_kb; kb++) {
    for (int nb = 0; nb < l_size_nb; nb++) {
      for (int bk = 0; bk < l_size_bk; bk++) {
        for (int bn = 0; bn < l_size_bn; bn++) {
          l_y[nb * l_size_bn + bn][kb * l_size_bk + bk] = l_blocked_y[kb][nb][bk][bn];
        }
      }
    }
  }

  // X: nb x cb x bc x bn
  // W: kb x cb x bk x bc
  // Y: kb x nb x bk x bn


  // check solution
  at::Tensor l_reference = at::matmul( l_x, l_w );
  REQUIRE( at::allclose( l_y, l_reference ) );
}
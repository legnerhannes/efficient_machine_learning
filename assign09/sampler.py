import torch
import simple_data_set

torch.distributed.init_process_group('mpi')
l_data = simple_data_set.SimpleDataSet(24)

l_rank = torch.distributed.get_rank()


# TASK 9.2 - 1

l_dsampler = torch.utils.data.distributed.DistributedSampler(l_data, num_replicas = 5, shuffle = True, drop_last = False)
print("REPLICA\tIN ", l_rank, ":", l_dsampler.num_replicas)
print("RANK\tIN ", l_rank, ":",l_dsampler.rank)
print("LEN\tIN ", l_rank, ":", len(l_dsampler))





# TASK 9.2 - 2

l_bsampler = torch.utils.data.BatchSampler(l_dsampler, batch_size = 9, drop_last = True)
print("BATCH S\tIN ", l_rank, ":",l_bsampler.batch_size)



import torch

torch.distributed.init_process_group('mpi')


# TASK 9.1 - 1
l_rank = torch.distributed.get_rank()
l_size = torch.distributed.get_world_size()

print(l_rank, l_size)

# TASK 9.1 - 2
if (l_rank == 0):
    l_data = torch.ones((3, 4))

    for i in range(1, l_size):
        l_req = torch.distributed.send(tensor = l_data, dst = i)

else:
    l_data = torch.zeros((3, 4))
    torch.distributed.recv(tensor = l_data, src = 0)


print(l_data)



# TASK 9.1 - 3

l_reqs = []

if (l_rank == 0):
    l_data_2 = torch.ones((3, 4))

    for i in range(1, l_size):
        l_reqs.append(torch.distributed.isend(tensor = l_data_2, dst = i))

else:
    l_data_2 = torch.zeros((3, 4))
    l_reqs.append(torch.distributed.irecv(tensor = l_data_2, src = 0))

for l_req in l_reqs:
    l_req.wait()



print(l_data_2)



# TASK 9.1 - 4

l_data_3 = torch.tensor(  [ [1,  2,  3,  4],
                            [5,  6,  7,  8],
                            [9, 10, 11, 12] ])

torch.distributed.all_reduce( l_data_3,
                              op = torch.distributed.ReduceOp.SUM )

print(l_data_3)
import torch
import matplotlib.pyplot as plt
import numpy as np

from numpy import genfromtxt

class MyModel(torch.nn.Module):
	def __init__(self):
		super(MyModel, self).__init__()
		self.linear = torch.nn.Linear(3, 1)

	def forward(self, input):
		output = self.linear(input)
		return output


# 3.1 Data Exploration
# methode for basic visualization of data
# projection needs to be set to 3d and x, y and z values for scattering the ponts are expected seperately
def plotData(data):
	x_values = [values[0] for values in data]
	y_values = [values[1] for values in data]
	z_values = [values[2] for values in data]

	fig = plt.figure()
	ax = plt.axes(projection='3d')
	ax.scatter3D(x_values, y_values, z_values)
	plt.show()

	

# MAIN FUNC

# load data
data = genfromtxt("./csv_files/data_points.csv", delimiter=',')
label = genfromtxt("./csv_files/data_labels.csv", delimiter=',')

data_t = torch.tensor(data, dtype = torch.float32)

label_t = torch.tensor(label, dtype = torch.float32)
label_t = label_t.unsqueeze(-1)

count = data_t.size()[0]


# 3.2 Dataset and Data Loaders
# Wraping the data of the tensor object data_t into a dataset variable dataset_t
dataset_t = torch.utils.data.TensorDataset(data_t)

# list to try multiple data_loader with different batch sizes
data_loader = []

# adds data loader with batch size 1, 5 and 10
data_loader.append(torch.utils.data.DataLoader(dataset_t))
data_loader.append(torch.utils.data.DataLoader(dataset_t, batch_size = 5))
data_loader.append(torch.utils.data.DataLoader(dataset_t, batch_size = 10))

# shows that the loader ouputs batches of data in the given size above
for i in range(len(data_loader)):
	print("Data Loader " + str(i))

	for batch in enumerate(data_loader[i]):
		print(batch)



# 3.3 Training
# creates a new Model object from the model class above
model = MyModel()

# create loss function and optimzer for the loop
loss_func = torch.nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr = 1E-1)

total_loss = 0

# for loop with 100 epochs that train the data and add to the total loss
for epoch in range(100):
	# makes prediction and calculates the loss of it
	# loss gets smaller in every iteration
	prediction = model(data_t)
	loss = loss_func(prediction, label_t)
	total_loss = total_loss + loss

	print(total_loss)

	# 3.4 Visualiazation
	# 3 epochs are shown and one of the point clouds get classiefed differently from the others
	if epoch == 30 or epoch == 60 or epoch == 90:
		fig = plt.figure()
		fig.suptitle('epoch ' + str(epoch))
		ax = plt.axes(projection='3d')
		ax.scatter3D(	data_t[prediction[:,0] < 0.5, 0],
						data_t[prediction[:,0] < 0.5, 1],
						data_t[prediction[:,0] < 0.5, 2])
		ax.scatter3D(	data_t[prediction[:,0] > 0.5, 0],
						data_t[prediction[:,0] > 0.5, 1],
						data_t[prediction[:,0] > 0.5, 2])
		plt.show()

	# setup for next loop
	optimizer.zero_grad()
	loss.backward()
	optimizer.step()

